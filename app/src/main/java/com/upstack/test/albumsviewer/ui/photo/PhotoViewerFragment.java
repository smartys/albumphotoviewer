package com.upstack.test.albumsviewer.ui.photo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.upstack.test.albumsviewer.BaseApplication;
import com.upstack.test.albumsviewer.R;
import com.upstack.test.albumsviewer.network.model.Photo;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Sergii on 27.09.2017.
 */

public class PhotoViewerFragment extends Fragment {

    public static String TAG = PhotoViewerFragment.class.getCanonicalName();
    private static String ALBUM_ID_KEY = "albumId";
    private static int GRID_COLUMN = 2;

    @Bind(R.id.photos_recycler_view)
    RecyclerView mPhotoRecyclerView;

    private PhotoAdapter photoAdapter;

    public static PhotoViewerFragment newInstance(String albumId) {
        PhotoViewerFragment f = new PhotoViewerFragment();
        Bundle args = new Bundle();
        args.putString(ALBUM_ID_KEY, albumId);
        f.setArguments(args);
        return f;
    }

    public String getAlbumId() {
        return getArguments().getString(ALBUM_ID_KEY, "1");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View v = inflater.inflate(R.layout.photo_viewer_layout, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mPhotoRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), GRID_COLUMN));
        photoAdapter = new PhotoAdapter(Collections.<Photo>emptyList());
        mPhotoRecyclerView.setAdapter(photoAdapter);
        loadPhotoData();
    }

    private void loadPhotoData() {
        BaseApplication.instance().getAlbumsService().getPhotos(getAlbumId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Photo>>() {
                    @Override
                    public void call(List<Photo> photoList) {
                        photoAdapter.swapDetailsData(photoList);
                    }
                });
    }

}
