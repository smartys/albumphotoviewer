package com.upstack.test.albumsviewer.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii on 27.09.2017.
 */

public class Photo {

    @SerializedName("albumId")
    private String albumId;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String photo;

    @SerializedName("thumbnailUrl")
    private String photoThumbnail;

    public Photo() {
    }

    public String getAlbumId() {
        return albumId;
    }

    public String getTitle() {
        return title;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhotoThumbnail() {
        return photoThumbnail;
    }
}
