package com.upstack.test.albumsviewer.network;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import retrofit.Retrofit;

/**
 * Created by Sergii on 27.09.2017.
 */

public class NetworkComponent {

    private static NetworkComponent instance;

    private final Context context;
    private final RetrofitManager retrofitManagerProvider;
    private final OkHttpManager okHttpManager;

    private IAlbumsService albumsService;
    private Retrofit retrofitManager;
    private OkHttpClient okHttpClient;

    public static NetworkComponent instance(
            Context context, RetrofitManager retrofitManager, OkHttpManager provider) {
        if (instance == null) {
            instance = new NetworkComponent(context, retrofitManager, provider);
        }
        return instance;
    }

    protected NetworkComponent(Context context, RetrofitManager retrofitManager,
                               OkHttpManager okHttpManager) {
        this.context = context;
        this.retrofitManagerProvider = retrofitManager;
        this.okHttpManager = okHttpManager;
    }

    public IAlbumsService getAlbumsService() {
        if (albumsService == null) {
            albumsService = getRetrofitManager().create(IAlbumsService.class);
        }
        return albumsService;
    }

    private Retrofit getRetrofitManager() {
        if (retrofitManager == null) {
            retrofitManager = retrofitManagerProvider.initRetrofit(getOkHttpClient());
        }
        return retrofitManager;
    }

    private OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            okHttpClient = okHttpManager.provideOkHttpClient(
                    okHttpManager.provideCookieManager(),
                    okHttpManager.provideCache(context));
        }
        return okHttpClient;
    }

}
