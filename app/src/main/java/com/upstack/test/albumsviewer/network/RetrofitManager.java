package com.upstack.test.albumsviewer.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by Sergii on 27.09.2017.
 */

public class RetrofitManager {

    private static final String BASE_URL = "http://jsonplaceholder.typicode.com/albums";

    public <T> T createRetrofitService(Retrofit retrofitManager, Class<T> serviceClass) {
        return retrofitManager.create(serviceClass);
    }

    public Retrofit initRetrofit(OkHttpClient okHttpClient) {
        /* This section is needed for logging of retrofit */
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient.interceptors().add(interceptor);
        /* end of the retrofit logging */
        final Gson gson = new GsonBuilder().create();

        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }
}
