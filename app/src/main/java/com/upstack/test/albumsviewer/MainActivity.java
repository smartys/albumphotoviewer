package com.upstack.test.albumsviewer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.upstack.test.albumsviewer.network.model.Album;
import com.upstack.test.albumsviewer.ui.albums.AlbumsAdapter;
import com.upstack.test.albumsviewer.ui.photo.PhotoViewerFragment;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.albums_list)
    RecyclerView mAlbumsRecyclerView;

    AlbumsAdapter albumsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAlbumsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        albumsAdapter = new AlbumsAdapter(Collections.<Album>emptyList(), createClickListener());
        mAlbumsRecyclerView.setAdapter(albumsAdapter);
        loadAlbumsData();
    }

    private AlbumsAdapter.IClickAlbumListener createClickListener() {
        return new AlbumsAdapter.IClickAlbumListener() {
            @Override
            public void clickAction(String albumId) {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, PhotoViewerFragment.newInstance(albumId))
                        .addToBackStack(PhotoViewerFragment.TAG)
                        .commit();
            }
        };
    }

    private void loadAlbumsData() {
        BaseApplication.instance().getAlbumsService().getAlbums()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Album>>() {
                    @Override
                    public void call(List<Album> albums) {
                        albumsAdapter.swapAlbumData(albums);
                    }
                });
    }
}
