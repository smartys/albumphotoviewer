package com.upstack.test.albumsviewer.network;

import com.upstack.test.albumsviewer.network.model.Album;
import com.upstack.test.albumsviewer.network.model.Photo;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by Sergii on 27.09.2017.
 */

public interface IAlbumsService {

    String ALBUMS_URL = "albums";
    String PHOTOS_URL = "albums/{albumId}/photos";

    @GET(ALBUMS_URL)
    Observable<List<Album>> getAlbums();

    @GET(PHOTOS_URL)
    Observable<List<Photo>> getPhotos(@Path("albumId") String albumId);
}
