package com.upstack.test.albumsviewer.network;

import android.content.Context;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

/**
 * Created by Sergii on 27.09.2017.
 */

public class OkHttpManager {

    private static final int HTTP_CACHE_SIZE = 10 * 1024 * 1024; // 10 Mb
    private static final String REST_API_CACHE_DIR_NAME = "rest_api_cache";

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_SEC = 120;
    private static final int DEFAULT_READ_TIMEOUT_IN_SEC = 120;

    public OkHttpClient provideOkHttpClient(CookieHandler cookieHandler, Cache cache) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(DEFAULT_READ_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.setCookieHandler(cookieHandler);
        if (cache != null) {
            okHttpClient.setCache(cache);
        }
        return okHttpClient;
    }

    public CookieManager provideCookieManager() {
        return new CookieManager();
    }

    public Cache provideCache(Context context) {
        File cacheDir = context.getDir(REST_API_CACHE_DIR_NAME, Context.MODE_PRIVATE);
        return new Cache(cacheDir, HTTP_CACHE_SIZE);
    }

}
