package com.upstack.test.albumsviewer;

import android.app.Application;

import com.squareup.picasso.Picasso;
import com.upstack.test.albumsviewer.network.IAlbumsService;
import com.upstack.test.albumsviewer.network.NetworkComponent;
import com.upstack.test.albumsviewer.network.OkHttpManager;
import com.upstack.test.albumsviewer.network.RetrofitManager;

/**
 * Created by Sergii on 27.09.2017.
 */

public class BaseApplication extends Application {

    private static BaseApplication instance;
    private NetworkComponent networkComponent;

    public static BaseApplication instance() {
        return instance;
    }

    public static Picasso getPicasso() {
        return Picasso.with(instance);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initRetrofit();
    }

    private void initRetrofit() {
        networkComponent = NetworkComponent.instance(this, new RetrofitManager(), new OkHttpManager());
    }

    public IAlbumsService getAlbumsService() {
        return networkComponent.getAlbumsService();
    }

}
