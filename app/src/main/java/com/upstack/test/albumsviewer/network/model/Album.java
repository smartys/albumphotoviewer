package com.upstack.test.albumsviewer.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii on 27.09.2017.
 */

public class Album {

    @SerializedName("userId")
    private String userId;

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    public Album() {
    }

    public String getUserId() {
        return userId;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
