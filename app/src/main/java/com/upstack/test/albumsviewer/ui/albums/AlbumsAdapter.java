package com.upstack.test.albumsviewer.ui.albums;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.upstack.test.albumsviewer.R;
import com.upstack.test.albumsviewer.network.model.Album;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Sergii on 27.09.2017.
 */

public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.AlbumViewHolder> {
    List<Album> albumsData;
    IClickAlbumListener listener;


    public AlbumsAdapter(List<Album> albumsData, IClickAlbumListener clickCardListener) {
        this.albumsData = new ArrayList<>(albumsData);
        listener = clickCardListener;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View albumItemView = inflater.inflate(R.layout.album_item_layout, parent, false);
        return new AlbumViewHolder(albumItemView, listener);
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, int position) {
        holder.bindData(albumsData.get(position));
    }

    @Override
    public int getItemCount() {
        return albumsData.size();
    }

    public void swapAlbumData(List<Album> albumList) {
        albumsData.clear();
        albumsData.addAll(albumList);
        notifyDataSetChanged();
    }

    public static class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.album_card_view)
        View mAlbumContainer;

        @Bind(R.id.album_title_text_view)
        TextView mTitleTextView;

        IClickAlbumListener cardListener;
        Album currentAlbum;

        public AlbumViewHolder(View categoryItemView, IClickAlbumListener listener) {
            super(categoryItemView);
            ButterKnife.bind(this, categoryItemView);
            cardListener = listener;
            mTitleTextView.setOnClickListener(this);
            mAlbumContainer.setOnClickListener(this);
        }

        public void bindData(Album albumToBind) {
            currentAlbum = albumToBind;
            mTitleTextView.setText(albumToBind.getTitle());
        }

        @Override
        public void onClick(View v) {
            if (!(currentAlbum == null)) {
                cardListener.clickAction(currentAlbum.getId());
            }
        }
    }

    public interface IClickAlbumListener {
        void clickAction(String albumId);
    }
}