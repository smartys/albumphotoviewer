package com.upstack.test.albumsviewer.ui.photo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.upstack.test.albumsviewer.BaseApplication;
import com.upstack.test.albumsviewer.R;
import com.upstack.test.albumsviewer.network.model.Photo;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Sergii on 27.09.2017.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {
    List<Photo> photos;

    public PhotoAdapter(List<Photo> photos) {
        this.photos = new ArrayList<>(photos);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View photoItemView = inflater.inflate(R.layout.photo_item_layout, parent, false);
        return new PhotoViewHolder(photoItemView);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bindData(photos.get(position));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void swapDetailsData(List<Photo> photos) {
        this.photos.clear();
        this.photos.addAll(photos);
        notifyDataSetChanged();
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.photo_image_view)
        ImageView mPhotoImage;

        @Bind(R.id.photo_title)
        TextView mPhotoTitle;

        Context context;
        Photo currentPhoto;

        public PhotoViewHolder(View categoryItemView) {
            super(categoryItemView);
            ButterKnife.bind(this, categoryItemView);
            context = categoryItemView.getContext();
        }

        public void bindData(Photo photoToBind) {
            mPhotoTitle.setText(photoToBind.getTitle());
            currentPhoto = photoToBind;
            BaseApplication.getPicasso().load(photoToBind.getPhoto())
                    .into(mPhotoImage);
        }
    }
}
